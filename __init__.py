from flask import Flask
from flask import request
import requests
import json
import os
from pymongo import Connection
from bson import json_util
from bson.objectid import ObjectId
from fbook import Fbook
from twitter import twitter

# Build the variables that use the assigned environment variables
#HOST = os.environ['OPENSHIFT_MONGODB_DB_HOST']
#PORT = int(os.environ['OPENSHIFT_MONGODB_DB_PORT'])
#DB_USER = os.environ['OPENSHIFT_MONGODB_DB_USERNAME']
#DB_PWD = os.environ['OPENSHIFT_MONGODB_DB_PASSWORD']
#DB_URL = os.environ['OPENSHIFT_MONGODB_DB_URL']
DB_NAME = 'api' #data base name

# app.config['PROPAGATE_EXCEPTIONS'] = True

muri = "mongodb://127.0.0.1:27017"
print muri
mconn = Connection(muri)
db = mconn[DB_NAME]

# Create the instance of Flask
app = Flask(__name__)


@app.route('/api/v1/users/<email>', methods = ['GET'])
def get_users(email):
    #Get a list of users
    usr = db.users.find({"email":email})
    usrList = []
    for u in usr:
        usrList.append(u)

    ul = json.dumps(usrList,default=json_util.default)
    print ul
    return ul

@app.route('/api/v1/users/create', methods = ['POST'])
def create_users():

    if not request.json:
        err = {"error":1}

    #Get a list of users
    user = {
        'name':request.json['name'],
        'email':request.json['email'],
        'company':request.json['company'],
        'password':request.json['password'],
        'twhandle':request.json['twhandle'],
        'fbbookid':request.json['fbbookid']
    }
    print user
    usr = db.users.insert(user)
    s = {'error':0}
    return json.dumps(s, default=json_util.default)



@app.route('/api/v1/swagtap/create',methods=['POST'])
def create_swagtap():

    if not request.json:
        err = {"error":1}
    #get a list of swaggertap
    
    swagtap= {
        #'objID':request.json['objID'],
        'name':request.json['name'],
        'startDate':request.json['startDate'],
        'endDate':request.json['endDate'],
        'twhandle':request.json['twhandle'],
        'fbbookid':request.json['fbbookid'],
        'event':request.json['event'],
        'instructions':request.json['instructions'],
        'hashTags':request.json['hashTags']
    }    
    print swagtap
    st = db.swagtap.insert(swagtap)
    s={'error':0}
    return json.dumps(s,default=json_util.default)

@app.route('/api/v1/swagtap/list',methods = ['GET'])
def get_swagtaps():
    swags = db.swagtap.find()
    swagList = []
    for s in swags:
        swagList.append(s)
    sl = json.dumps(swagList,default=json_util.default)
    return sl

@app.route('/api/v1/swagtap/<objID>', methods = ['GET'])
def get_swagtap(objID):
    #Get a list of users
    usr = db.swagtap.find({"_id": ObjectId(objID)})
    usrList = []
    for u in usr:
        tHash = u.get("hashTags")
        tempArray = []
        #fbook = Fbook(tHash)
        #stuff = fbook.getPosts()
        #for r in stuff:
        #    tempArray.append(r)
        twits = twitter(tHash)
        stuff2 = twits.twitter()
        for r in stuff2:
            tempArray.append(r)
        u.update({"media" : tempArray})
        usrList.append(u)
    ul = json.dumps(usrList,default=json_util.default)
    print ul
    return ul

    


# @app.route("/")
# def index():
#     obj = {"user":"dylan"}

#     db.users.insert(obj)
#     num = db.users.find({"user":"dylan"})
#     j = ""
#     for i in num:
#         print i
#         j = i['user']

   

#     return j

if __name__ == "__main__":

    app.run(debug="True")

