class swagTap(object):
	"""docstring for swagTap"""
	def __init__(self, arg):
		super(swagTap, self).__init__()
		self.objID = ""
		self.name = ""
		self.startDate = ""
		self.endDate = ""
		self.twHandle = ""
		self.fbID = ""
		self.instructions = []
		self.event = ""
		self.hashTags=[]
	def getObjID(self):
		return 	# get objID from mongodb

	def setName(self,name):
		self.name =name

	def getName(self):
		return self.name

	def getStartDate(self):
		return self.startDate

	def setStartDate(self,startdate):
		self.startdate = startdate

	def setEndDate(self,enddate):
		self.endDate=enddate
		
	def getEndDate(self):
		return self.endDate	

	def setTwHandler(self,twHandle):
		self.twHandle = twHandle

	def getTwHandler(self):
		return self.twHandle

	def setFbID(self,fbID):
		self.fbID = fbID

	def getFbID(self):
		return self.fbID

	def setInstructions(self,instructions):
		self.instructions = instructions

	def getInstructions(self):
		return	self.instructions

	def getEvent(self):
		return self.event

	def setEvent(self,event):
		self.event=event

	def getHashTags(self):
		return self.hashTags

	def setHashTags(self,hashtags):
		self.hashTags = hashtags
