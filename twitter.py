import requests
import json
import os
from TwitterSearch import *

'''
[{"userName": "dyang_pointio", "socialType": "Twitter", "photoLink": "https://pbs.twimg.com/media/BhHU-CMCUAAP82K.jpg", "hashTag": "#Drexelswagger", "geo": [39.95615784, -75.19236379], "message": "#Drexelswagger geo test http://t.co/MTqF6slX4x", "userID": 1411893078, "ID": 437364801195352064, "dateTime": "Sat Feb 22 23:14:29 +0000 2014"}]
'''

class twitter(object):

   def __init__(self,hashtag):
        self.hashtag = hashtag
   def twitter(self):
        hashtag = self.hashtag
        #hashtag ="#Philadelphia"
        tso = TwitterSearchOrder() # create a TwitterSearchOrder object
        tso.setKeywords([hashtag]) # let's define all words we would like to have a look for
        # tso.setLanguage('en') # we want to see German tweets only
        # tso.setCount(100) # please dear Mr Twitter, only give us 7 results per page
        # tso.setIncludeEntities(False) # and don't give us all those entity information
    
        # it's about time to create a TwitterSearch object with our secret tokens
    
        ts = TwitterSearch(
            consumer_key = '<your consumeer key',
            consumer_secret = 'your consumer secret',
            access_token = 'your access token',
            access_token_secret = 'your access token secret'
         )
        jsonArray=[]
        jsonElements=dict()
    
    
        for tweet in ts.searchTweetsIterable(tso): # this is where the fun actually starts :)
            jsonElements = {}
            jsonElements.update({"ID": tweet['id']})
            jsonElements.update({"socialType": "Twitter"})
            jsonElements.update({"hashTag": hashtag})
            jsonElements.update({"dateTime": tweet['created_at']})
            #INPUT USERID
    
            if tweet['user']['id'] == None:
                jsonElements.update({"userID": None})
            else:
                jsonElements.update({"userID": tweet['user']['id']})
    
            #INPUT NAME
            if tweet['user']['screen_name']== None:
                jsonElements.update({"userName": None})
            else:
                jsonElements.update({"userName": tweet['user']['screen_name']})
    
            #INPUT PLACE - Check if existing in the post - Not Showing in Photos for Some Odd Reason
            if tweet['geo'] == None:
                jsonElements.update({"geo": None})
            else:
                jsonElements.update({"geo": tweet['geo']['coordinates']})
    
            #Last way to enter to message data into json object
            if not tweet['entities'].has_key("media"):
                jsonElements.update({"photoLink": None})
            else:
                for i in tweet['entities']['media']:
                    jsonElements.update({"photoLink":i['media_url_https']})
    
            if tweet['text'] == None:
                jsonElements.update({"message": None})
            else:
                jsonElements.update({"message": tweet['text']})
            jsonArray.append(jsonElements)


        return jsonArray

