import requests
import json

class Fbook():
    def __init__(self,hashtag):
        self.hashtag = hashtag
    def getPosts(self):
        #Vars Fo Access
        hashtag = self.hashtag
        fbaccesscode = "your fb access code"
        limit = "100"
    
        req = requests.get('https://graph.facebook.com/search?type=post&q=%23' + hashtag + '&access_token=' + fbaccesscode + '&limit=' + limit)
        resp = req.json()
        jsonArray = []
        jsonElements = dict()
        for value in resp.get("data"):
            jsonElements.update({"ID": value.get("id")})
            jsonElements.update({"socialType": "FB"})
            jsonElements.update({"hashTag": hashtag})
            jsonElements.update({"dateTime": value.get("created_time")})
            #INPUT USERID
            if value.get("from").get("id") == None:
                jsonElements.update({"userID": None})
            else:
                jsonElements.update({"userID": value.get("from").get("id")})
            
            #INPUT NAME
            if value.get("from").get("name") == None:
                jsonElements.update({"userName": None})
            else:    
                jsonElements.update({"userName": value.get("from").get("name")})
            
            #INPUT PLACE - Check if existing in the post - Not Showing in Photos for Some Odd Reason
            if value.get("place") == None:
                jsonElements.update({"geo": None})
            else:
                if value.get("place").get("location").get("latitude") == None:
                    jsonElements.update({"geo": getLat})
                elif value.get("place").get("location").get("longitude") == None:
                    jsonElements.update({"geo": getLat})
                else:
                    getLat = "[" + str(value.get("place").get("location").get("latitude")) + "," + str(value.get("place").get("location").get("longitude")) +  "]"
                    jsonElements.update({"geo": getLat})
            #Last way to enter to message data into json object
            if value.get("type") == 'status':
                jsonElements.update({"photoLink": None})
                if value.get("message") == None:
                    jsonElements.update({"message": None})
                else:
                    jsonElements.update({"message": value.get("message")})
                jsonArray.append(jsonElements)
                jsonElements = {}
            elif value.get("type") == 'photo':
                if value.get("picture") == None:
                    jsonElements.update({"photoLink": None})
                else:
                    jsonElements.update({"photoLink": value.get("picture")})
                if value.get("caption") == None:
                    jsonElements.update({"message": None})
                else:
                    jsonElements.update({"message": value.get("caption")})
                jsonArray.append(jsonElements)
                jsonElements = {}
            else:
                #Don't Add To Array
                #print("howdy")
                jsonElements = {}
        
        return jsonArray