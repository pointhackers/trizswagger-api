__author__ = 'dylan'

class user(object):
    ''' docstring for User'''
    def __init__(self, arg):
        super(user, self).__init__()
        self.name =""
        self.password=""
        self.email=""
        self.company=""
        self.twHandle=""
        self.fbookID=""
    
    def setName(self,name):
        self.name=name

    def getName(self):
        return self.name

    def setPwd(self,password):
        self.password = password

    def getPwd(self):
        return self.password

    def setEmail(self,email):
        self.email=email

    def getEmail(self):
        return self.email

    def setCompany(self,company):
        self.company=company

    def getCompany(self):
        return self.company

    def setTwHandle(self,twHandle):
        self.twHandle=twHandle

    def getTwHandle(self):
        return self.twHandle
    
    def setFbookID(self,fbookID):
        self.fbookID=fbookID
    
    def getFbookID(self):
        return self.fbookID                   
